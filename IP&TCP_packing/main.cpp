#include <iostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <algorithm>

//To do: replace all raw pointers with smart pointers

#pragma pack(push,1)
struct PackP1 {
	uint16_t size;
	uint16_t id;
};
#pragma pack(pop)

uint8_t* create_package(std::fstream& stream, int id, int msize)
{
	uint8_t * buffer;	
	buffer = new uint8_t[sizeof(PackP1) + msize];
	PackP1* pack = (PackP1*)buffer;
	char * data = (char*)(pack + 1); //SP1 returns adress. Moving + 1 on adress, moves as many bytes as size of a type written there
	/*The above is the solution. In current situation SP1 points to first part of buffer (place for struct)
	and d points to second part of buffer just behind SP1 (place for data)*/

	stream.read(data, msize);
	PackP1 package;
	package.id = id;
	package.size = msize;
	*pack = package;

	return buffer;
}

bool comparePtrToStruct(uint8_t* a, uint8_t* b)
{
	PackP1* p1 = (PackP1*)a;
	PackP1* p2 = (PackP1*)b;
	return (p1->id < p2->id);
}

void print_vec(const std::vector<uint8_t*>& packages)
{
	
	for (size_t j = 0; j < packages.size(); j++)
	{
		PackP1* pack = (PackP1*)packages[j];
		for (size_t i = 0; i < (sizeof(PackP1) + pack->size); ++i) {
			std::cout << (int)*(packages[j] + i) << " ";
		}
		std::cout << std::endl;
	}
}

int main()
{
	int length;
	std::fstream stream("test.txt", std::fstream::binary | std::fstream::in | std::fstream::out);

	if (stream)
	{
		//get iterator to end of stream
		stream.seekg(0, stream.end);
		length = stream.tellg();
		//get iterator back to beggining
		stream.seekg(0, stream.beg);	//We get a warning here. Why?

		std::cout << "get messedge size: ";
		int messedge_size;
		std::cin >> messedge_size;
		int last_size = length % messedge_size;
		int n = (length - last_size) / messedge_size;

		int id = 0;
		std::vector<uint8_t*> packages;
		for ( id; id < n; id++)
		{
			packages.push_back(create_package(stream, id, messedge_size));
		}

		if (last_size != 0)
		{
			packages.push_back(create_package(stream, id, last_size));
		}

		std::vector<uint8_t*> unsorted;
		for (size_t i = packages.size(); i > 0; --i)
		{
			unsorted.push_back(packages[(i-1)]);
		}
		
		std::cout << "base packs: \n";
		print_vec(packages);
		std::cout << "unsorted packs: \n";
		print_vec(unsorted);
		std::cout << "\nsorting packs...\n\n";
		std::sort(unsorted.begin(), unsorted.end(), comparePtrToStruct);
		std::cout << "sorted packs: \n";
		print_vec(unsorted);

		stream.close();
	}
	
	std::cin.clear();
	std::cin.ignore(1);
	std::cin.get();
}
	
//protok� modelu ISO OSI
//Czym sie rozni kompilacja od linkowania
//uruchomic biblioteke pcap albo npcap